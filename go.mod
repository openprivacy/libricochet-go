module git.openprivacy.ca/openprivacy/libricochet-go

go 1.14

require (
	cwtch.im/tapir v0.1.18
	git.openprivacy.ca/openprivacy/connectivity v1.1.1
	git.openprivacy.ca/openprivacy/log v1.0.0
	github.com/golang/protobuf v1.3.5
	golang.org/x/crypto v0.0.0-20200420104511-884d27f42877
)
