package connection

import (
	"crypto/rand"
	"crypto/rsa"
	"git.openprivacy.ca/openprivacy/libricochet-go/identity"
	"git.openprivacy.ca/openprivacy/libricochet-go/utils"
	"golang.org/x/crypto/ed25519"
	"net"
	"testing"
	"time"
)

// Server
func ServerAuthValid(hostname string, publicKey rsa.PublicKey) (allowed, known bool) {
	return true, true
}

// Server
func ServerAuthValid3DH(hostname string, publicKey ed25519.PublicKey) (allowed, known bool) {
	return true, true
}

func TestProcessAuthAs3DHServer(t *testing.T) {

	ln, _ := net.Listen("tcp", "127.0.0.1:0")

	pub, priv, _ := ed25519.GenerateKey(rand.Reader)

	go func() {
		cconn, _ := net.Dial("tcp", ln.Addr().String())

		cpub, cpriv, _ := ed25519.GenerateKey(rand.Reader)

		hostname := utils.GetTorV3Hostname(pub)
		orc := NewOutboundConnection(cconn, hostname)

		known, err := HandleOutboundConnection(orc).ProcessAuthAsV3Client(identity.InitializeV3("", &cpriv, &cpub))
		if err != nil {
			t.Errorf("Error while testing ProcessAuthAsClient (in ProcessAuthAsServer) %v", err)
			return
		} else if !known {
			t.Errorf("Client should have been known to the server, instead known was: %v", known)
			return
		}
	}()

	conn, _ := ln.Accept()

	rc := NewInboundConnection(conn)
	err := HandleInboundConnection(rc).ProcessAuthAsV3Server(identity.InitializeV3("", &priv, &pub), ServerAuthValid3DH)
	if err != nil {
		t.Errorf("Error while testing ProcessAuthAsServer: %v", err)
	}

	// Wait for server to finish
	time.Sleep(time.Second * 2)

	// Test Close
	rc.Close()
}

func TestProcessAuthAsV3ServerFail(t *testing.T) {

	ln, _ := net.Listen("tcp", "127.0.0.1:0")

	pub, priv, _ := ed25519.GenerateKey(rand.Reader)

	go func() {
		cconn, _ := net.Dial("tcp", ln.Addr().String())

		cpub, cpriv, _ := ed25519.GenerateKey(rand.Reader)

		// Setting the RemoteHostname to the client pub key approximates a server sending the wrong public key.
		hostname := utils.GetTorV3Hostname(cpub)
		orc := NewOutboundConnection(cconn, hostname)

		HandleOutboundConnection(orc).ProcessAuthAsV3Client(identity.InitializeV3("", &cpriv, &cpub))
	}()

	conn, _ := ln.Accept()

	rc := NewInboundConnection(conn)
	err := HandleInboundConnection(rc).ProcessAuthAsV3Server(identity.InitializeV3("", &priv, &pub), ServerAuthValid3DH)
	if err == nil {
		t.Errorf("Error while testing ProcessAuthAsServer: %v", err)
	}
}

func TestProcessAuthAsV3ClientFail(t *testing.T) {

	ln, _ := net.Listen("tcp", "127.0.0.1:0")

	pub, priv, _ := ed25519.GenerateKey(rand.Reader)

	go func() {
		cconn, _ := net.Dial("tcp", ln.Addr().String())

		// Giving the client inconsistent keypair to make EDH fail
		cpub, _, _ := ed25519.GenerateKey(rand.Reader)
		_, cpriv, _ := ed25519.GenerateKey(rand.Reader)

		hostname := utils.GetTorV3Hostname(pub)
		orc := NewOutboundConnection(cconn, hostname)

		HandleOutboundConnection(orc).ProcessAuthAsV3Client(identity.InitializeV3("", &cpriv, &cpub))
	}()

	conn, _ := ln.Accept()

	rc := NewInboundConnection(conn)
	err := HandleInboundConnection(rc).ProcessAuthAsV3Server(identity.InitializeV3("", &priv, &pub), ServerAuthValid3DH)
	if err == nil {
		t.Errorf("Error while testing ProcessAuthAsServer: %v", err)
	}
}

func TestProcessAuthTimeout(t *testing.T) {

	ln, _ := net.Listen("tcp", "127.0.0.1:0")

	go func() {
		net.Dial("tcp", ln.Addr().String())
		time.Sleep(17 * time.Second)

	}()

	conn, _ := ln.Accept()

	pub, priv, _ := ed25519.GenerateKey(rand.Reader)
	rc := NewInboundConnection(conn)
	err := HandleInboundConnection(rc).ProcessAuthAsV3Server(identity.InitializeV3("", &priv, &pub), ServerAuthValid3DH)
	if err != utils.ActionTimedOutError {
		t.Errorf("Error while testing TestProcessAuthTimeout - Should have timed out after 15 seconds, instead ERR was %v", err)
	}
}
