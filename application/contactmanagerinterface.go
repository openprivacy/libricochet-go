package application

import (
	"crypto/rsa"
	"golang.org/x/crypto/ed25519"
)

// ContactManagerInterface provides a mechanism for autonous applications
// to make decisions on what connections to accept or reject.
type ContactManagerInterface interface {
	LookupContact(hostname string, publicKey rsa.PublicKey) (allowed, known bool)
	LookupContactV3(hostname string, publicKey ed25519.PublicKey) (allowed, known bool)
}
