package application

// AcceptAllContactHandler is a pass through Contact Handler. It is currently only used by the integration test.
// TODO: DEPRECATE
type AcceptAllContactHandler struct{}

// ContactRequest returns "Pending" for everything
func (aach *AcceptAllContactHandler) ContactRequest(name string, message string) string {
	return "Pending"
}

// ContactRequestRejected is a noop
func (aach *AcceptAllContactHandler) ContactRequestRejected() {
}

// ContactRequestAccepted is a noop
func (aach *AcceptAllContactHandler) ContactRequestAccepted() {
}

// ContactRequestError is a noop
func (aach *AcceptAllContactHandler) ContactRequestError() {
}
