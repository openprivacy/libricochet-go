package application

import (
	"crypto/rsa"
	"golang.org/x/crypto/ed25519"
)

// AcceptAllContactManager implements the contact manager interface an presumes
// all connections are allowed.
// It is currently used by the Cwtch Server.
// TODO Deprecate
type AcceptAllContactManager struct {
}

// LookupContact returns that a contact is known and allowed to communicate for all cases.
func (aacm *AcceptAllContactManager) LookupContact(hostname string, publicKey rsa.PublicKey) (allowed, known bool) {
	return true, true
}

// LookupContactV3 returns that a contact is known and allowed to communicate for all cases.
func (aacm *AcceptAllContactManager) LookupContactV3(hostname string, publicKey ed25519.PublicKey) (allowed, known bool) {
	return true, true
}

// ContactRequest accepts every single Contact Request
func (aacm *AcceptAllContactManager) ContactRequest(name string, message string) string {
	return "Accepted"
}
