// Code generated by protoc-gen-go. DO NOT EDIT.
// source: Auth3EDH.proto

/*
Package Protocol_Data_Auth_TripleEDH is a generated protocol buffer package.

It is generated from these files:
	Auth3EDH.proto

It has these top-level messages:
	Packet
	Proof
	Result
*/
package Protocol_Data_Auth_TripleEDH

import proto "github.com/golang/protobuf/proto"
import fmt "fmt"
import math "math"
import Protocol_Data_Control "git.openprivacy.ca/openprivacy/libricochet-go/wire/control"

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

type Packet struct {
	Proof            *Proof  `protobuf:"bytes,1,opt,name=proof" json:"proof,omitempty"`
	Result           *Result `protobuf:"bytes,2,opt,name=result" json:"result,omitempty"`
	XXX_unrecognized []byte  `json:"-"`
}

func (m *Packet) Reset()         { *m = Packet{} }
func (m *Packet) String() string { return proto.CompactTextString(m) }
func (*Packet) ProtoMessage()    {}

func (m *Packet) GetProof() *Proof {
	if m != nil {
		return m.Proof
	}
	return nil
}

func (m *Packet) GetResult() *Result {
	if m != nil {
		return m.Result
	}
	return nil
}

type Proof struct {
	Proof            []byte `protobuf:"bytes,1,opt,name=proof" json:"proof,omitempty"`
	XXX_unrecognized []byte `json:"-"`
}

func (m *Proof) Reset()         { *m = Proof{} }
func (m *Proof) String() string { return proto.CompactTextString(m) }
func (*Proof) ProtoMessage()    {}

func (m *Proof) GetProof() []byte {
	if m != nil {
		return m.Proof
	}
	return nil
}

type Result struct {
	Accepted         *bool  `protobuf:"varint,1,req,name=accepted" json:"accepted,omitempty"`
	IsKnownContact   *bool  `protobuf:"varint,2,opt,name=is_known_contact,json=isKnownContact" json:"is_known_contact,omitempty"`
	XXX_unrecognized []byte `json:"-"`
}

func (m *Result) Reset()         { *m = Result{} }
func (m *Result) String() string { return proto.CompactTextString(m) }
func (*Result) ProtoMessage()    {}

func (m *Result) GetAccepted() bool {
	if m != nil && m.Accepted != nil {
		return *m.Accepted
	}
	return false
}

func (m *Result) GetIsKnownContact() bool {
	if m != nil && m.IsKnownContact != nil {
		return *m.IsKnownContact
	}
	return false
}

var E_ClientPublicKey = &proto.ExtensionDesc{
	ExtendedType:  (*Protocol_Data_Control.OpenChannel)(nil),
	ExtensionType: ([]byte)(nil),
	Field:         9200,
	Name:          "Protocol.Data.Auth.TripleEDH.client_public_key",
	Tag:           "bytes,9200,opt,name=client_public_key,json=clientPublicKey",
	Filename:      "Auth3EDH.proto",
}

var E_ClientEphmeralPublicKey = &proto.ExtensionDesc{
	ExtendedType:  (*Protocol_Data_Control.OpenChannel)(nil),
	ExtensionType: ([]byte)(nil),
	Field:         9300,
	Name:          "Protocol.Data.Auth.TripleEDH.client_ephmeral_public_key",
	Tag:           "bytes,9300,opt,name=client_ephmeral_public_key,json=clientEphmeralPublicKey",
	Filename:      "Auth3EDH.proto",
}

var E_ServerPublicKey = &proto.ExtensionDesc{
	ExtendedType:  (*Protocol_Data_Control.ChannelResult)(nil),
	ExtensionType: ([]byte)(nil),
	Field:         9200,
	Name:          "Protocol.Data.Auth.TripleEDH.server_public_key",
	Tag:           "bytes,9200,opt,name=server_public_key,json=serverPublicKey",
	Filename:      "Auth3EDH.proto",
}

var E_ServerEphmeralPublicKey = &proto.ExtensionDesc{
	ExtendedType:  (*Protocol_Data_Control.ChannelResult)(nil),
	ExtensionType: ([]byte)(nil),
	Field:         9300,
	Name:          "Protocol.Data.Auth.TripleEDH.server_ephmeral_public_key",
	Tag:           "bytes,9300,opt,name=server_ephmeral_public_key,json=serverEphmeralPublicKey",
	Filename:      "Auth3EDH.proto",
}

func init() {
	proto.RegisterType((*Packet)(nil), "Protocol.Data.Auth.TripleEDH.Packet")
	proto.RegisterType((*Proof)(nil), "Protocol.Data.Auth.TripleEDH.Proof")
	proto.RegisterType((*Result)(nil), "Protocol.Data.Auth.TripleEDH.Result")
	proto.RegisterExtension(E_ClientPublicKey)
	proto.RegisterExtension(E_ClientEphmeralPublicKey)
	proto.RegisterExtension(E_ServerPublicKey)
	proto.RegisterExtension(E_ServerEphmeralPublicKey)
}
